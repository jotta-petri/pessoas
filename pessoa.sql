﻿CREATE DATABASE IF NOT EXISTS `pessoa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;


USE `pessoa`;




DROP TABLE IF EXISTS `pessoas`;


CREATE TABLE IF NOT EXISTS `pessoas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  
`name` varchar(100) NOT NULL,
  
`doc` varchar(11) NOT NULL,
  
`phone` varchar(20) NOT NULL,
  
`birth` date NOT NULL,
  
`created` date NOT NULL,
 
`modified` date NOT NULL,
  
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


INSERT INTO `pessoas` (`id`, `name`, `doc`, `phone`, `birth`, `created`, `modified`) 
VALUES
(1, 'João', '123', '111', '2014-01-13', '2019-02-05', '2019-02-15'),

(2, 'Maria', '321', '22', '2019-02-05', '2019-02-05', '2019-02-05'),

(3, 'juca', '0000', '01', '2019-02-05', '2019-02-05', '2019-02-15'),

(4, 'Ana', '231', '123', '2019-02-05', '2019-02-05', '2019-02-05'),

(5, 'Marcela', '1234', '9', '2019-02-12', '2019-02-12', '2019-02-12'),
(6, 'José', '888', '777', '2019-02-15', '2019-02-15', '2019-02-15');


COMMIT;

